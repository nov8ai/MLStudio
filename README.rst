=======================
Machine Learning Studio
=======================


.. image:: https://img.shields.io/pypi/v/ml_studio.svg
        :target: https://pypi.python.org/pypi/ml_studio

.. image:: https://img.shields.io/travis/decisionscients/ml_studio.svg
        :target: https://travis-ci.org/decisionscients/ml_studio

.. image:: https://readthedocs.org/projects/ml-studio/badge/?version=latest
        :target: https://ml-studio.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://pyup.io/repos/github/decisionscients/ml_studio/shield.svg
     :target: https://pyup.io/repos/github/decisionscients/ml_studio/
     :alt: Updates



Python Boilerplate contains all the boilerplate you need to create a Python package.


* Free software: BSD license
* Documentation: https://ml-studio.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
