# =========================================================================== #
#                             GRADIENT DESCENT                                #
# =========================================================================== #
"""This is the 'blackbox' for gradient descent optimization classes.

Captures and reports information for a single optimization, such as:
    
    * Summary Information - Parameters, data, duration, iterations etc...
    * Cost Information - Costs, and associated parameters for each update
    * Scores - Training and validation set scores 

"""
import datetime
import numpy as np
import pandas as pd
class Blackbox():
    """Blackbox class which stores summary and detail optimization information."""

    def __init__(self):
        """Initializes the event logs."""

        # Parameters
        self._params = None

        # Data
        self._X = None
        self._y = None
        self._X_val = None
        self._y_val = None

        # Summary information
        self._alg = None
        self._start = None
        self._end =None    
        self._duration = None
        self._epochs = None
        self._iterations = None
        self._initial_cost = None
        self._final_cost = None
        self._final_thetas = None
        self._complete = False

        # Log Costs
        self._costs_epochs = []
        self._costs_iterations = []
        self._costs_learning_rates = []
        self._costs_thetas = []
        self._costs = []

        # Log Training and Validation Scores
        self._scores_epochs = []
        self._scores_iterations = []
        self._scores_train_score = []
        self._scores_val_score = []

    def log_init(self, alg, params, X, y, X_val=None, y_val=None):
        """Logs initialization 

        Logs general information about the optimization including the name
        of the algorithm, the hyperparameters and start time.

        Arguments:
        ----------
            alg (str): The name of the algorithm
            params (dict): Dictionary containing the hyperparameters
            X (nd.array): Input features
            y (nd.array): Output values
            X_val (nd.array): Validation set features
            y_val (nd.array): Validation set outputs

        """
        self._alg = alg     
        self._params = params  
        self._X = X
        self._y = y
        self._X_val = X_val
        self._y_val = y_val 
        self._start = datetime.datetime.now()
        self._complete = False
        return(self)

    def log_complete(self):
        """Logs end of optimization"""
        self._end = datetime.datetime.now()
        self._duration = (self._end-self._start).total_seconds()
        self._epochs = self._costs_epochs[-1]
        self._iterations = self._costs_iterations[-1]
        self._initial_cost = self._costs[0]
        self._final_cost = self._costs[-1]
        self._final_thetas = self._costs_thetas[-1]
        self._complete = True
        return(self)

    def log_costs(self, epoch, iteration, learning_rate, theta, cost):
        """Updates Event A Log
        
        Arguments:
        ----------
            epoch (int): Current epoch
            iteration (int): Current iteration. Will differ from epoch for 
                mini-batch and stochastic gradient descent.
            learning_rate (float): Current learning rate used for 
                adaptive or annealing learning rates.
            theta (np.array): Array of parameters theta.
            cost (float): Cost associated with parameters theta
        
        """

        self._costs_epochs.append(epoch)
        self._costs_iterations.append(iteration)
        self._costs_learning_rates.append(learning_rate)
        self._costs_thetas.append(theta)
        self._costs.append(cost)
        return(self)
        
    def log_scores(self, epoch, iteration, train_score, val_score):
        """Updates Event A Log

        Arguments:
        ----------
            epoch (int): Current epoch
            iteration (int): Current iteration. Will differ from epoch for 
                mini-batch and stochastic gradient descent.
            train_score (float): Training set score
            val_score (float): Validation set score
        
        """
        self._scores_epochs.append(epoch)
        self._scores_iterations.append(iteration)
        self._scores_train_score.append(train_score)
        self._scores_val_score.append(val_score)
        return(self)

    def _get_label(self, x):
        labels = {'c': 'Constant Learning Rate',
                  't': 'Time Decay Learning Rate',
                  's': 'Step Decay Learning Rate',
                  'e': 'Exponential Decay Learning Rate'}
        return(labels.get(x,x))  

    def _todf(self, x, stub):
        """ Converts a list of n-dimensional arrays to dataframe"""
        n = len(x[0])
        df = pd.DataFrame()
        for i in range(n):
            colname = stub + str(i)
            vec = [item[i] for item in x]
            df_vec = pd.DataFrame(vec, columns=[colname])
            df = pd.concat([df, df_vec], axis=1)
        return(df)         

    def _get_thetas(self, initial=False, final=False):
        """Returns requested parameters theta from history"""
        if initial:
            thetas = self._todf(self._costs_thetas, stub='theta_init_')
            thetas=thetas.head(1)
        elif final:
            thetas = self._todf(self._costs_thetas, stub='theta_final_')
            thetas=thetas.tail(1)
        else:
            thetas = self._todf(self._costs_thetas, stub='theta_')
        return(thetas)

    def _get_costs(self):
        detail = {'alg': self._alg,
                  'epoch': self._costs_epochs,
                  'iteration': self._costs_iterations,
                  'learning_rate': self._costs_learning_rates,
                  'cost': self._costs}
        detail = pd.DataFrame.from_dict(detail)      
        thetas = self._get_thetas()
        detail = pd.concat([detail, thetas], axis=1)        
        return(detail)

    def _get_scores(self):
        detail = {'alg': self._alg,
                  'epoch': self._scores_epochs,
                  'iteration': self._scores_iterations,
                  'train_score': self._scores_train_score,
                  'val_score': self._scores_val_score}
        detail = pd.DataFrame.from_dict(detail)      
        return(detail)     


    def _get_summary(self):        
        summary = pd.DataFrame({'alg': self._alg,
                                'start' : self._start,
                                'end' : self._end,
                                'duration': self._duration,
                                'epochs': self._epochs,
                                'iterations': self._iterations,
                                'initial_cost': self._initial_cost,
                                'final_cost': self._final_cost}, index=[0])
        thetas = self._get_thetas(final=True)
        thetas = thetas.reset_index(drop=True)
        summary = pd.concat([summary,  thetas], axis=1)        
        return(summary)

    def results(self):
        """Returns summary and detail results of gradient descent.

        Returns:
        --------
        Dict: Containing the summary and detail dataframes.  

        """
        if not self._complete:
            raise Exception("Model has not been fit. No log available.")        
        summary = self._get_summary()
        costs = self._get_costs()        
        scores = self._get_scores()        
        return({'params': self._params,
                'summary': summary, 'costs': costs, 'scores': scores,
                'X': self._X, 'y': self._y, 'X_val': self._X_val, 'y_val': self._y_val})         