# =========================================================================== #
#                                  SCORER MODULE                              #
# =========================================================================== #

import math
import numpy as np
import pandas as pd


# --------------------------------------------------------------------------- #
#                                 Scorer Class                                #
# --------------------------------------------------------------------------- #
class Scorer():
    """Computes performance metric given data and parameters"""

    def __init__(self):
        pass

    def _sse(self, y_true, y_pred):
        """Computes sum squared error given data and parameters

        Arguments:
        ----------
            y_true (nd.array): True output values
            y_pred (1d array): Predicted output values

        Returns:
        -------- 
            Float: Sum of squared errors

        """        
        e = y_true-y_pred
        return(np.sum(e**2))    

    def _sst(self, y_true, y_pred):
        """Computes total sum squared error given data and parameters
        (actual vs avg(actual))

        Arguments:
        ----------
            y_true (nd.array): True output values
            y_pred (1d array): Predicted output values

        Returns:
        -------- 
            Float: Total sum of squared errors

        """
        y_avg = np.mean(y_true)
        e = (y_true-y_avg)**2                
        return(np.sum(e**2))   

    def _r2(self, y_true, y_pred):
        """Computes coefficient of determination

        Arguments:
        ----------
            y_true (nd.array): True output values
            y_pred (1d array): Predicted output values

        Returns:
        -------- 
            Float: Coefficient of determination 

        """        
        r2 = 1 - self._sse(y_true, y_pred)/self._sst(y_true, y_pred)        
        return(r2)

    def _var_explained(self, y_true, y_pred):
        """Computes proportion of variance explained

        Arguments:
        ----------
            y_true (nd.array): True output values
            y_pred (1d array): Predicted output values

        Returns:
        -------- 
            Float: Coefficient of determination 

        """
        var_explained = 1 - np.var(y_true-y_pred) / np.var(y_true)
        return(var_explained)                   

    def _mse(self, y_true, y_pred):
        """Computes mean squared error given data and parameters

        Arguments:
        ----------
            y_true (nd.array): True output values
            y_pred (1d array): Predicted output values

        Returns:
        -------- 
            Float: Mean squared error

        """        
        e = y_true-y_pred
        return(np.mean(e**2))

    def _nmse(self, y_true, y_pred):
        """Computes negative mean squared error given data and parameters

        Arguments:
        ----------
            y_true (nd.array): True output values
            y_pred (1d array): Predicted output values

        Returns:
        -------- 
            Float: Negative mean squared error

        """        
        e = y_true-y_pred
        return(-np.mean(e**2))

    def _rmse(self, y_true, y_pred):
        """Computes root mean squared error given data and parameters

        Arguments:
        ----------
            y_true (nd.array): True output values
            y_pred (1d array): Predicted output values

        Returns:
        -------- 
            Float: Root mean squared error

        """        
        e = y_true-y_pred
        return(np.sqrt(np.mean(e**2)))


    def _nrmse(self, y_true, y_pred):
        """Computes negative root mean squared error given data and parameters

        Arguments:
        ----------
            y_true (nd.array): True output values
            y_pred (1d array): Predicted output values

        Returns:
        -------- 
            Float: Negative root mean squared error

        """        
        e = y_true-y_pred
        return(-np.sqrt(np.mean(e**2)))

    def score(self, y_true, y_pred, metric='neg_mean_squared_error'):
        """Computes and returns requested score for designated metric. 

        Arguments:
        ----------
            y_true (nd.array): The true output values
            y_pred (1d array): Predicted output values
            metric(str): The metric to score

        Returns:
        -------- 
            Float: score

        """
        dispatcher = {'r2': self._r2,
                      'var_explained': self._var_explained,
                      'mean_squared_error': self._mse,                      
                      'neg_mean_squared_error': self._nmse,
                      'root_mean_squared_error': self._rmse,
                      'neg_root_mean_squared_error': self._nrmse}
        return(dispatcher[metric](y_true, y_pred))
