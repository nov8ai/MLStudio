# =========================================================================== #
#                        Gradient Descent Base Class                          #
# =========================================================================== #

from abc import ABC, abstractmethod
import datetime
import math
import warnings

import numpy as np
from numpy import array
from numpy.random import RandomState
import pandas as pd
from sklearn.base import BaseEstimator
from sklearn.base import RegressorMixin
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

from .costfuncs import CostFunc
from .blackbox import Blackbox
from .predict import Predict
from .scorer import Scorer

warnings.filterwarnings("ignore", category=RuntimeWarning)
# --------------------------------------------------------------------------- #

class GradientDescent(ABC, BaseEstimator, RegressorMixin):
    '''Abstract base class for Gradient Descent

    GradientDescent provides the core functionality common among batch, 
    stochastic, and mini-batch gradient descent for both regression and 
    classification problems.  

    '''

    def __init__(self, fit_intercept=True, theta_init=None, learning_rate=0.01,  
                learning_rate_sched='c', decay_rate=0, step_epochs=10, 
                maxiter=2500, early_stop=True, batch_size=32,
                stop_metric='neg_mean_squared_error', precision=0.01, 
                epochs_stable=5, val_size=0.2, task='linear_regression', 
                cost='quadratic', random_state=None):        

        self.alg = "Gradient Descent Base Class"

        # Hyperparameters
        self._fit_intercept = fit_intercept
        self._theta_init = theta_init
        self._learning_rate = learning_rate
        self._learning_rate_sched = learning_rate_sched
        self._early_stop = early_stop
        self._precision = precision
        self._decay_rate = decay_rate
        self._step_epochs = step_epochs
        self._maxiter = maxiter
        self._epochs_stable = epochs_stable
        self._val_size = val_size
        self._batch_size = batch_size
        self._task = task
        self._cost = cost
        self._random_state = random_state

        # Initialize plug-ins
        self._scorer = None
        self._predictor = None
        self._cost_func = None

        # Initialize final parameters theta
        self._theta = None

        # Set validation set stop metric and initialize state vars
        self._stop_metric = stop_metric
        self._iter_no_improvement = 0
        self._best_val_score = None

    def get_params(self, deep=True):
        """Returns algorithm hyperparameters

        Arguments:
        ----------
            deep (bool): Required of sklearn base estimators. Default is True.

        Returns:
        --------
            Dict: Dictionary containing the estimators hyperparameters. 

        """
        
        return{'fit_intercept': self._fit_intercept,
               'theta_init' : self._theta_init,
               'learning_rate': self._learning_rate,
               'learning_rate_sched': self._learning_rate_sched, 
               'early_stop': self._early_stop,
               'stop_metric': self._stop_metric,
               'precision': self._precision,
               'decay_rate': self._decay_rate,
               'step_epochs': self._step_epochs,
               'val_size': self._val_size,
               'batch_size': self._batch_size,
               'maxiter': self._maxiter,
               'epochs_stable': self._epochs_stable,
               'task': self._task,
               'cost': self._cost,
               'random_state': self._random_state}

    def set_params(self, **parameters):
        """Sets algorithm hyperparameters

        Method used by sklearn's Pipeline class to set hyperparameters for
        the estimator

        Arguments:
        ----------
            **parameters (kwargs): Keyword arguments containing algorithm 
                hyperparameters
            
        """

        self._fit_intercept = parameters.get('fit_intercept')        
        self._theta_init = parameters.get('theta_init')
        self._learning_rate = parameters.get('learning_rate', 0.001)
        self._learning_rate_sched = parameters.get('learning_rate_sched', 'c')
        self._early_stop = parameters.get('early_stop', True)
        self._stop_metric = parameters.get('stop_metric', 'neg_mean_squared_error')
        self._decay_rate = parameters.get('decay_rate', 0)
        self._step_epochs = parameters.get('step_epochs', 10)
        self._val_size = parameters.get('val_size', 0.2)
        self._batch_size = parameters.get('batch_size', 32)
        self._precision = parameters.get('precision', 0.001)
        self._maxiter = parameters.get('maxiter', 5000)
        self._epochs_stable = parameters.get('epochs_stable', 5)
        self._task = parameters.get('task', 'linear_regression')
        self._cost = parameters.get('cost', 'quadratic')
        self._random_state = parameters.get('random_state', None)
        return self         

    def _init_thetas(self, X):
        seed = RandomState(50)
        if self._theta_init is None:
            theta = seed.normal(size=X.shape[1])
        else:
            theta = self._theta_init
        return(theta)   

    def _init_plugins(self):
        """Instantiates classes used for scoring, prediction, costs, etc..."""
        self._blackbox = Blackbox()
        self._scorer = Scorer()
        self._predictor = Predict().get_predict_func(self._task)
        self._cost_func = CostFunc().get_cost_func(self._cost)

    def _scores(self, epoch, iteration, X, y, X_val, y_val, theta):
        """Computes the designated metric on the training and validation sets."""
        train_score = val_score = None
        # Compute training set scores
        y_pred = self._predictor.predict(X, theta)
        train_score = self._scorer.score(y, y_pred, self._stop_metric)
        # Compute validation set scores (if early stopping)
        y_pred = self._predictor.predict(X_val, theta)
        val_score = self._scorer.score(y_val, y_pred, self._stop_metric)
        # Update blackbox with scores
        self._blackbox.log_scores(epoch, iteration, train_score, val_score)
        

    def _update_theta(self, theta, learning_rate, gradient):
        return(theta-(learning_rate * gradient))

    def _update_learning_rate(self, learning_rate, epoch):
        
        if self._learning_rate_sched == 'c':
            learning_rate_new = learning_rate
        elif self._learning_rate_sched == 't':
            k = self._decay_rate
            learning_rate_new = self._learning_rate/(1+k*epoch)            
        elif self._learning_rate_sched == 's':
            drop = self._decay_rate
            epochs_drop = self._step_epochs
            learning_rate_new = self._learning_rate*math.pow(drop, math.floor((1+epoch)/epochs_drop))
        elif self._learning_rate_sched == 'e':            
            k = self._decay_rate
            learning_rate_new = self._learning_rate * math.exp(-k*epoch)

        return(learning_rate_new)

    def _improvement(self, val_score):
        """Determines if improvement is made in validation set score.
        
        Arguments:
        ----------
            val_score (float): Score measuring performance on validation set

        Returns:
        --------
            bool: True if improvement is observed, False otherwise.
        
        """
        # If the best validation scores is None, this is the first iteration
        # and we set the number of consecutive iterations without improvement 
        # to 0 and return True.
        if self._best_val_score is None:
            self._best_val_score = val_score
            self._iter_no_improvement = 0
            return(True)

        # If the validation score is greater than the previous score plus
        # a margin of precision, we set the number of consecutive iterations 
        # without improvement to 0 and return True
        if val_score > abs(self._precision * self._best_val_score) + self._best_val_score:
            self._best_val_score = val_score
            self._iter_no_improvement = 0
            return(True)                    

        # Alternatively, increase the number of consecutive iterations 
        # without improvement and return False        
        self._iter_no_improvement += 1
        return(False)

    def _finished(self, X, y, theta, epoch):
        """Determines if the optimization is finished

        If the early stop parameter is set, the validation score is
        computed and passed to _improvement method to evaluate whether
        improvement in validation score was observed. If no improvement
        is observed in the designated number of epochs, the method
        returns True, indicating that the early stop criteria was
        met.
        
        If the early stop parameter is not set, the algorithm returns
        True if the designated maximum number of epochs has been reached.
        False otherwise.

        Arguments:
        ----------
            X (np.array): Validation set inputs
            y (np.array): Validation set outputs
            theta (np.array): Current parameters theta
            epoch (int): The current epoch

        Returns:
        --------
            Bool: True if early stopping criteria or maximum 
                designated epochs have been reached. False
                otherwise.
        """

        if self._early_stop:
            y_pred = self._predictor.predict(X, theta)
            val_score = self._scorer.score(y, y_pred, self._stop_metric)
            if self._improvement(val_score):                
                pass
            elif self._iter_no_improvement == self._epochs_stable:
                return(True)
    
        if self._maxiter:
            if epoch > self._maxiter:
                return(True)       
        return(False)

    @abstractmethod
    def fit(self, X, y):
        pass

    def predict(self, X):
        """Computes prediction based upon data and parameters theta.

        The prediction technique to be used is a parameter set 
        at initialization. Supported techniques includde linear regression
        and logistic regression.

        Arguments:
        ----------
            X (nd.array): Inputs
            theta (nd.array): Parameters theta
        
        Returns:
        --------
            nd.array of shape (X.shape[0], 1)

        """
        if self._theta is None:
            raise Exception("Must call 'fit' method before 'predict'.")

        return(self._predictor.predict(X,self._theta))

    def rmse(self, y_truth, y_predicted):
        """Computes root mean squared error

        Arguments:
        ----------
            y_truth (1d array): y values
            y_predicted (1d array): y predicted values
        
        Returns:
        --------
            Float: root mean squared error
        """
        e = y_truth - y_predicted
        return(np.sqrt(np.mean(e**2)))

    def results(self):
        """Reports results of optimization blackbox object.

        Results:
        --------
        Dict: Dictionary containing three DataFrames:
            1. Summary of optimization including parameters, hyperparameters,
                elapsed time, initial and final thetas.
            2. Log of cost history for each update
            3. Log of training and validation performance each epoch.   
        """
        return(self._blackbox.results())

# --------------------------------------------------------------------------- #
#                       Batch Gradient Descent Class                          #
# --------------------------------------------------------------------------- #
class BGD(GradientDescent):
    """Batch Gradient Descent"""

    def __init__(self, *args, **kwargs):        
        super(BGD, self).__init__(*args, **kwargs)
        self.alg = "Batch Gradient Descent"

    def fit(self,  X, y):
        """Fits the parameters to the data using batch gradient descent.

        The method receives the input variables X and the output vector, y,
        then initializes the counters. A bias term is added to the inputs
        if the 'fit_intercept' parameter is true. The thetas are initialized
        learning rate is set and the training set is split into a validation
        set used to evaluate early stopping (if selected).

        Arguments:
        ----------
            X (nd.array): Inputs array
            y (nd.array): Output vector 

        Attributes: 
        -----------
            intercept_ (float): The intercept term
            coef_ (nd.array): Coefficients

        """         

        # Initialize 'plug-in' classes
        self._init_plugins() 
        # Initialize counters, logs, thetas, learning rate and development sets
        epoch = iteration = 1        
        # The bias term is added to the inputs of 'fit_intercept' is True
        X = np.c_[np.ones(X.shape[0]), X] if self._fit_intercept else X
        # Randomly initialize thetas
        theta = self._init_thetas(X)
        # Initialize learning rate. 
        learning_rate = self._learning_rate
        # Allocate the designated proportion of the training set for validation         
        X, X_val, y, y_val = train_test_split(X,y, test_size=self._val_size,
                                                random_state=self._random_state)    
        # The log is initialized with the name of the algorithm and the parameters
        self._blackbox.log_init(self.alg, self.get_params(), X, y, X_val, y_val)                                                       

        while not self._finished(X_val, y_val, theta, epoch):

            # Compute costs and update history 
            cost = self._cost_func.cost(X, y, theta)            
            self._blackbox.log_costs(epoch, iteration, learning_rate, theta, cost)      

            # Compute training and validation scores and update blackbox
            self._scores(epoch, iteration, X, y, X_val, y_val, theta)
            
            # Compute gradient and update thetas
            g = self._cost_func.gradient(X, y, theta)
            theta = self._update_theta(theta, learning_rate, g)
            
            # Update learning rate (if required)
            learning_rate = self._update_learning_rate(learning_rate, epoch)

            iteration += 1
            epoch += 1

        # Close log
        self._blackbox.log_complete()
        self._theta = theta
        self.intercept_ = theta[0]
        self.coef_ = theta[1:]
        return self

# --------------------------------------------------------------------------- #
#                      Stochastic Gradient Descent                            #
# --------------------------------------------------------------------------- #            
class SGD(GradientDescent):
    """Stochastic Gradient Descent"""

    def __init__(self, *args, **kwargs):
        super(SGD, self).__init__(*args, **kwargs)
        self.alg = "Stochastic Gradient Descent Regression"

    def fit(self,  X, y): 

        # Initialize 'plug-in' classes
        self._init_plugins() 
        # Initialize counters, logs, thetas, learning rate and development sets
        epoch = iteration = 1
        # The bias term is added to the inputs of 'fit_intercept' is True
        X = np.c_[np.ones(X.shape[0]), X] if self._fit_intercept else X
        # Randomly initialize thetas
        theta = self._init_thetas(X)
        # Initialize learning rate. 
        learning_rate = self._learning_rate
        # Allocate the designated proportion of the training set for validation 
        X, X_val, y, y_val = train_test_split(X,y, test_size=self._val_size,
                                                random_state=self._random_state)  
        # Initialize blackbox with algorithm, parameters, and data
        self._blackbox.log_init(self.alg, self.get_params(), X, y, X_val, y_val)        

        while not self._finished(X_val, y_val, theta, epoch):
            
            # Shuffle data and iterate through the training examples
            X, y = shuffle(X, y)

            for x_i, y_i in zip(X,y):
                # Compute cost on training example and update log
                cost = self._cost_func.cost(X, y, theta)            
                self._blackbox.log_costs(epoch, iteration, learning_rate, theta, cost) 

                # compute gradient on single example and update thetas
                g = self._cost_func.gradient(x_i, y_i, theta)
                theta = self._update_theta(theta, learning_rate, g)                              
                iteration += 1

            # Compute training and validation scores and update blackbox
            self._scores(epoch, iteration, X, y, X_val, y_val, theta)
            
            # Update learning rate (if required for annealing strategies)
            learning_rate = self._update_learning_rate(learning_rate, epoch)

            epoch += 1        

        # Close log
        self._blackbox.log_complete()  
        self._theta = theta          
        self.intercept_ = theta[0]
        self.coef_ = theta[1:]
        return self        

# --------------------------------------------------------------------------- #
#                      MINI-BATCH Gradient Descent                            #
# --------------------------------------------------------------------------- #            
class MBGD(GradientDescent):
    '''Minibatch Gradient Descent'''

    def __init__(self, *args, **kwargs):
        super(MBGD, self).__init__(*args, **kwargs)
        self.alg = "Minibatch Gradient Descent"

    def _batch(self, x, n):
        for i in range(0, len(x), n):
            yield x[i:i+n]     

    def fit(self,  X, y): 

        # Initialize 'plug-in' classes
        self._init_plugins() 
        # Initialize counters, logs, thetas, learning rate and development sets
        epoch = iteration = 1
        # The bias term is added to the inputs of 'fit_intercept' is True
        X = np.c_[np.ones(X.shape[0]), X] if self._fit_intercept else X
        # Randomly initialize thetas
        theta = self._init_thetas(X)
        # Initialize learning rate. 
        learning_rate = self._learning_rate
        # Allocate the designated proportion of the training set for validation 
        X, X_val, y, y_val = train_test_split(X,y, test_size=self._val_size,
                                                random_state=self._random_state)  
        # The log is initialized with the name of the algorithm and the parameters
        self._blackbox.log_init(self.alg, self.get_params(), X, y, X_val, y_val)                                                  

        while not self._finished(X_val, y_val, theta, epoch):

            # Shuffle data and create batches 
            X, y = shuffle(X, y)
            X_batches = self._batch(X, n=self._batch_size)
            y_batches = self._batch(y, n=self._batch_size)

            for x_mb, y_mb in zip(X_batches,y_batches):

                # Compute cost on training example and update log
                cost = self._cost_func.cost(X, y, theta)            
                self._blackbox.log_costs(epoch, iteration, learning_rate, theta, cost)                 

                # Compute gradient and udpate thetas
                g = self._cost_func.gradient(x_mb, y_mb, theta)
                theta = self._update_theta(theta, learning_rate, g)                              
                iteration += 1

            # Compute training and validation scores and update blackbox
            self._scores(epoch, iteration, X, y, X_val, y_val, theta)
            
            # Update learning rate (if required for annealing strategies)
            learning_rate = self._update_learning_rate(learning_rate, epoch)

            epoch += 1   

        # Close log
        self._blackbox.log_complete() 
        self._theta = theta           
        self.intercept_ = theta[0]
        self.coef_ = theta[1:]
        return self
