# =========================================================================== #
#                            PREDICT MODULE                                   #
# =========================================================================== #
"""Encapsulates regression prediction techniques"""

import numpy as np
import pandas as pd
# --------------------------------------------------------------------------- #
#                        LinearRegression Class                               #
# --------------------------------------------------------------------------- #
class LinearRegression():
    """Computes predictions using the linear regression formula """

    def __init__(self):
        pass

    def predict(self, X, theta):
        """Computes predictions for inputs X, given parameters theta

        Arguments:
        ----------
            X (nd.array): X inputs
            theta(nd.array): Parameters theta

        Returns:
        --------
            nd.array of predictions

        """
        return(X.dot(theta))

class LogisticRegression():
    """Computes predictions using the logistic regression formula """

    def __init__(self):
        pass

    def predict(self, X, theta):
        """Computes predictions for inputs X, given parameters theta

        Arguments:
        ----------
            X (nd.array): X inputs
            theta(nd.array): Parameters theta

        Returns:
        --------
            nd.array of predictions

        """
        t = X.dot(theta)

        return(1/(1+np.exp(-t)))    

class Predict():
    """Class returns class that performs selected prediction function"""

    def __init__(self):
        """Initializes dictionary of supported prediction function classes"""

        self._predict = {'linear_regression': LinearRegression(),
                         'logistic_regression': LogisticRegression()}        

    def get_predict_func(self, func):
        """Returns class corresponding to the requested prediction function

        Arguments:
        ----------
            func (str): String containing one of the supported prediction functions
        
        Returns:
        --------
            class: Instance of class containing the requested prediction function
                implementation
        
        """
        return(self._predict[func])        
