# %%
# =========================================================================== #
#                               GRADIENT VISUAL                               #
# =========================================================================== #
"""Visualization classes for diagnostics and evaluation"""
# --------------------------------------------------------------------------- #
import datetime
from IPython.display import HTML
import itertools
from itertools import zip_longest
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')
import matplotlib.animation as animation
import matplotlib.gridspec as gridspec
from matplotlib.animation import FuncAnimation
from matplotlib import cm
from matplotlib import animation, rc, rcParams
from matplotlib import colors as mcolors
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import axes3d, Axes3D
import numpy as np
from numpy import array, newaxis
import pandas as pd
import seaborn as sns
import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)

from ...utils.filemanager import save_fig, save_csv, save_gif

# --------------------------------------------------------------------------- #
#                               GradientDiagnostics                           #   
# --------------------------------------------------------------------------- #
class GradientDiagnostics:
    """Renders diagnostic plots""" 

    def __init__(self):
        pass

    def learning_curve(self, model, directory=None, filename=None):
        """Renders learning curve e.g. costs by epoch"""
    
        # Unpack model results
        results = model.results()   
        params = results['params']     
        summary = results['summary']
        costs = results['costs']
        alg = summary['alg'].iloc[0]
        title = alg + "\n" + "Learning Curve" + "\n" + \
                "\n" + r'$\alpha = $' + str(round(params['learning_rate'],3))

        # Initialize and render plot
        fig, ax = plt.subplots(figsize=(12,4))       
        sns.set(style="whitegrid", font_scale=1)

        # Plot train and validation scores
        ax = sns.lineplot(x='epoch', y='cost', data=costs, ax=ax)
        ax.set_facecolor('w')
        ax.tick_params(colors='k')
        ax.xaxis.label.set_color('k')
        ax.yaxis.label.set_color('k')
        ax.set_xlabel("Epoch")
        ax.set_ylabel("Costs")
        ax.set_title(title, color='k')
        plt.show()  

        if directory is not None:
            if filename is None:
                filename = alg + ' Learning Curve ' + str(params['learning_rate']) +  '.png'
            save_fig(fig, directory, filename)        
        
        return(fig)    

    def validation_curve(self, model, directory=None, filename=None):
        """Renders validation showing scores for training and validation sets"""
    
        # Unpack model results
        results = model.results()   
        params = results['params']     
        summary = results['summary']
        scores = results['scores']
        alg = summary['alg'].iloc[0]
        stop_metric = params['stop_metric'].replace("_", " ")
        stop_metric = stop_metric.capitalize()
        title = alg + "\n" + "Validation Curve" + "\n" + stop_metric + \
                "\n" + r'$\alpha = $' + str(round(params['learning_rate'],3))

        # Format data for plotting
        df_wide = scores[['epoch', 'train_score', 'val_score']]
        df = pd.melt(df_wide, id_vars=['epoch'], var_name='dataset', value_name='scores')

        # Initialize and render plot
        fig, ax = plt.subplots(figsize=(12,4))       
        sns.set(style="whitegrid", font_scale=1)

        # Plot train and validation scores
        ax = sns.lineplot(x='epoch', y='scores', hue='dataset', legend='full',  data=df, ax=ax)
        ax.set_facecolor('w')
        ax.tick_params(colors='k')
        ax.xaxis.label.set_color('k')
        ax.yaxis.label.set_color('k')
        ax.set_xlabel("Epoch")
        ax.set_ylabel(stop_metric)
        ax.set_title(title, color='k')
        plt.show()  

        if directory is not None:
            if filename is None:
                filename = alg + ' Learning Curve ' + str(params['learning_rate']) +  '.png'
            save_fig(fig, directory, filename)        
        
        return(fig)         


# --------------------------------------------------------------------------- #
#                             SingleOptimationSearch3D                        #  
# --------------------------------------------------------------------------- #
class SingleOptimationSearch3D:
    """Animates search for 2D optimization in 3D"""    

    def __init__(self):
        pass        

    def _cost_mesh(self,X, y, THETA):
        return(np.sum((X.dot(THETA) - y)**2)/(2*len(y)))        

    def search(self, model, directory=None, filename=None, fontsize=None,
                    interval=200, secs=10, maxframes=100):
        '''Animates gradient descent for 2D problems in 3D.
        '''        

        # Unpack model results
        results = model.results()   
        params = results['params']     
        summary = results['summary']
        detail = results['costs']
        X = results['X']
        y = results['y']
        alg = summary['alg'].iloc[0]

        # Designate plot area
        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(111, projection='3d')
        sns.set(style="whitegrid", font_scale=1)

        # Create index for n <= maxframes number of points
        idx = np.arange(0,detail.shape[0])
        nth = math.floor(detail.shape[0]/maxframes)
        nth = max(nth,1) 
        idx = idx[::nth]

        # Create the x=theta0, y=theta1 grid for plotting
        iterations = detail['iteration']
        costs = detail['cost']     
        theta0 = detail['theta_0']
        theta1 = detail['theta_1']

        # Format frames per second
        fps = math.floor(len(iterations)/secs) if len(iterations) >=secs else math.floor(secs/len(iterations))      

        # Establish boundaries of plot
        theta0_min = min(-1, min(theta0))
        theta1_min = min(-1, min(theta1))
        theta0_max = max(1, max(theta0))
        theta1_max = max(1, max(theta1))
        theta0_mesh = np.linspace(theta0_min, theta0_max, 100)
        theta1_mesh = np.linspace(theta1_min, theta1_max, 100)
        theta0_mesh, theta1_mesh = np.meshgrid(theta0_mesh, theta1_mesh)

        # Create cost grid based upon x,y the grid of thetas
        Js = np.array([self._cost_mesh(X, y, THETA)
                    for THETA in zip(np.ravel(theta0_mesh), np.ravel(theta1_mesh))])
        Js = Js.reshape(theta0_mesh.shape)

        # Set Title
        plt.rc('text', usetex=True)
        title = alg + '\n' + r'$\alpha$' + " = " + str(round(params['learning_rate'],3))
        if fontsize:
            ax.set_title(title, color='k', pad=30, fontsize=fontsize)                            
            display = ax.text2D(0.1,0.92, '', transform=ax.transAxes, color='k', fontsize=fontsize)
        else:
            ax.set_title(title, color='k', pad=30)               
            display = ax.text2D(0.3,0.92, '', transform=ax.transAxes, color='k')             
        # Set face, tick,and label colors 
        ax.set_facecolor('w')
        ax.tick_params(colors='k')
        ax.xaxis.label.set_color('k')
        ax.yaxis.label.set_color('k')
        # make the panes transparent
        ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        # make the grid lines transparent
        ax.xaxis._axinfo["grid"]['color'] =  (1,1,1,0)
        ax.yaxis._axinfo["grid"]['color'] =  (1,1,1,0)
        ax.zaxis._axinfo["grid"]['color'] =  (1,1,1,0)
        # Make surface plot
        ax.plot_surface(theta0_mesh, theta1_mesh, Js, rstride=1,
                cstride=1, cmap='jet', alpha=0.5, linewidth=0)
        ax.set_xlabel(r'Intercept($\theta_0$)')
        ax.set_ylabel(r'Slope($\theta_1$)')
        ax.set_zlabel('Cost')        
        ax.view_init(elev=30., azim=30)

        # Build the empty line plot at the initiation of the animation
        line3d, = ax.plot([], [], [], 'r-', label = 'Gradient descent', lw = 1.5)
        line2d, = ax.plot([], [], [], 'b-', label = 'Gradient descent', lw = 1.5)
        point3d, = ax.plot([], [], [], 'bo')
        point2d, = ax.plot([], [], [], 'bo')

        def init():

            # Initialize 3d line and point
            line3d.set_data([],[])
            line3d.set_3d_properties([])
            point3d.set_data([], [])
            point3d.set_3d_properties([])

            # Initialize 2d line and point
            line2d.set_data([],[])
            line2d.set_3d_properties([])
            point2d.set_data([], [])
            point2d.set_3d_properties([])

            # Initialize display
            display.set_text('')
            return (line2d, point2d, line3d, point3d, display,)

        # Animate the regression line as it converges
        def animate(i):
            # Animate 3d Line
            line3d.set_data(theta0[:idx[i]], theta1[:idx[i]])
            line3d.set_3d_properties(costs[:idx[i]])

            # Animate 3d points
            point3d.set_data(theta0[idx[i]], theta1[idx[i]])
            point3d.set_3d_properties(costs[idx[i]])

            # Animate 2d Line
            line2d.set_data(theta0[:idx[i]], theta1[:idx[i]])
            line2d.set_3d_properties(0)

            # Animate 2d points
            point2d.set_data(theta0[idx[i]], theta1[idx[i]])
            point2d.set_3d_properties(0)

            # Update display
            display.set_text('Iteration: '+ str(iterations[idx[i]]) + r'$\quad\theta_0=$ ' +
                            str(round(theta0[idx[i]],3)) + r'$\quad\theta_1=$ ' + str(round(theta1[idx[i]],3)) +
                            ' Cost: ' + str(np.round(costs[idx[i]], 5)))


            return(line3d, point3d, line2d, point2d, display)

        # create animation using the animate() function
        surface_ani = animation.FuncAnimation(fig, animate, init_func=init, frames=len(idx),
                                            interval=interval, blit=True, repeat_delay=3000)
        if directory is not None:
            if filename is None:
                filename = alg + ' Search Path Learning Rate ' + str(params['learning_rate']) +  '.gif'
            save_gif(surface_ani, directory, filename, fps)
        plt.close(fig)
        return(surface_ani)

# --------------------------------------------------------------------------- #
#                              SingleOptimationFit3D                          #   
# --------------------------------------------------------------------------- #
class SingleOptimationFit2D:
    """Animates gradient descent fit in 2D for 2D optimation problem."""

    def __init__(self):
        pass        


    def fit(self, model, directory=None, filename=None, fontsize=None,
                 interval=200, secs=10, maxframes=100):

        # Unpack model results
        results = model.results()   
        summary = results['summary']
        detail = results['costs']
        X = results['X']
        y = results['y']
        alg = summary['alg'].iloc[0]

        # Create index for n <= maxframes number of points
        idx = np.arange(0,detail.shape[0])
        nth = math.floor(detail.shape[0]/maxframes)
        nth = max(nth,1) 
        idx = idx[::nth]

        # Extract data for plotting
        x = X[:,1]
        iterations = detail['iteration']
        cost = detail['cost']        
        theta0 = detail['theta_0']
        theta1 = detail['theta_1']
        theta = np.array([theta0, theta1])

        # Format frames per second
        fps = math.floor(len(iterations)/secs) if len(iterations) >=secs else math.floor(secs/len(iterations))
        
        # Render scatterplot
        fig, ax = plt.subplots(figsize=(12,8))
        sns.set(style="whitegrid", font_scale=1)
        sns.scatterplot(x=x, y=y, ax=ax)
        # Set face, tick,and label colors 
        ax.set_facecolor('w')
        ax.tick_params(colors='k')
        ax.xaxis.label.set_color('k')
        ax.yaxis.label.set_color('k')
        # Initialize line
        line, = ax.plot([],[],'r-', lw=2)
        # Set Title, Annotations and label
        title = alg + '\n' + r' $\alpha$' + " = " + str(round(detail['learning_rate'].iloc[0],3)) 
        if fontsize:
            ax.set_title(title, color='k', fontsize=fontsize)                                    
            display = ax.text(0.1, 0.9, '', transform=ax.transAxes, color='k', fontsize=fontsize)
        else:
            ax.set_title(title, color='k')                                    
            display = ax.text(0.35, 0.9, '', transform=ax.transAxes, color='k')
        ax.set_xlabel('X')
        ax.set_ylabel('y')
        fig.tight_layout()

        # Build the empty line plot at the initiation of the animation
        def init():
            line.set_data([],[])
            display.set_text('')
            return (line, display,)

        # Animate the regression line as it converges
        def animate(i):

            # Animate Line
            y=X.dot(theta[:,idx[i]])
            line.set_data(x,y)

            # Animate text
            display.set_text('Iteration: '+ str(iterations[idx[i]]) + r'$\quad\theta_0=$ ' +
                            str(round(theta0[idx[i]],3)) + r'$\quad\theta_1=$ ' + str(round(theta1[idx[i]],3)) +
                            ' Cost: ' + str(round(cost[idx[i]], 3)))
            return (line, display)

        # create animation using the animate() function
        line_gd = animation.FuncAnimation(fig, animate, init_func=init, frames=len(idx),
                                            interval=interval, blit=True, repeat_delay=3000)
        if directory is not None:
            if filename is None:
                filename = title = alg + ' Fit Plot Learning Rate ' + str(round(detail['learning_rate'].iloc[0],3)) + '.gif'  
            save_gif(line_gd, directory, filename, fps)
        plt.close(fig)  
        return(line_gd)

# --------------------------------------------------------------------------- #
#                            MultiOptimationSearch3D                          #  
# --------------------------------------------------------------------------- #
class MultiOptimationSearch3D(animation.FuncAnimation):
    """Animates gradient descent in 3D for multiple 2D optimization problems."""

    def __init__(self):
        pass

    def _anim8(self, *paths, zpaths, methods=[], frames=None, 
                 interval=60, repeat_delay=5, blit=False, **kwargs):

        self.paths = paths
        self.zpaths = zpaths

        if frames is None:
            frames = max(path.shape[1] for path in paths)
            self.nth = [1 for p in paths]
        else:
            self.nth = [max(1,math.floor(path.shape[1] / frames)) for path in paths]
        cmap = plt.get_cmap('jet')
        colors = cmap(np.linspace(0,1,len(paths)))
        self.lines = [self.ax.plot([], [], [], alpha=0.7, label=method, c=c, lw=2)[0] 
                      for _, method, c in zip_longest(paths, methods, colors)]

        ani = animation.FuncAnimation(self.fig, self._update, init_func=self._init,
                                      frames=frames, interval=interval, blit=blit,
                                      repeat_delay=repeat_delay, **kwargs)
        self.ax.legend(loc='upper left')                                      
        return(ani)                                                  

    def _init(self):
        for line in self.lines:
            line.set_data([], [])
            line.set_3d_properties([])
        return self.lines

    def _update(self, i):
        for line, nth, path, zpath in zip(self.lines, self.nth, self.paths, self.zpaths):
            self.ax.view_init(elev=10., azim=i*.30)
            line.set_data(*path[::,:i*nth])
            line.set_3d_properties(zpath[:i*nth])
        return self.lines

    def _cost_mesh(self,X, y, THETA):
        return(np.sum((X.dot(THETA) - y)**2)/(2*len(y))) 

    def _surface(self, X, y, models):

        # Designate plot area
        self.fig = plt.figure(figsize=(12, 8))
        self.ax = self.fig.add_subplot(111, projection='3d')
        sns.set(style="whitegrid", font_scale=1)

        # Get minimum and maximum thetas and establish boundaries of plot
        theta0_min = min([min(v.results()['costs']['theta_0']) for (k,v) in models.items()])
        theta1_min = min([min(v.results()['costs']['theta_1']) for (k,v) in models.items()])
        theta0_max = max([max(v.results()['costs']['theta_0']) for (k,v) in models.items()])
        theta1_max = max([max(v.results()['costs']['theta_1']) for (k,v) in models.items()])
        theta0_mesh = np.linspace(theta0_min, theta0_max, 100)
        theta1_mesh = np.linspace(theta1_min, theta1_max, 100)
        theta0_mesh, theta1_mesh = np.meshgrid(theta0_mesh, theta1_mesh)

        # Create cost grid based upon x,y the grid of thetas
        Js = np.array([self._cost_mesh(X, y, THETA) 
                    for THETA in zip(np.ravel(theta0_mesh), np.ravel(theta1_mesh))])
        Js = Js.reshape(theta0_mesh.shape)

        # Set Title
        title = 'Gradient Descent Trajectories'
        self.ax.set_title(title, color='k', pad=30)                       

        # Set face, tick,and label colors 
        self.ax.set_facecolor('w')
        self.ax.tick_params(colors='k')
        self.ax.xaxis.label.set_color('k')
        self.ax.yaxis.label.set_color('k')
        # make the panes transparent
        self.ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        self.ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        self.ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        # make the grid lines transparent
        self.ax.xaxis._axinfo["grid"]['color'] =  (1,1,1,0)
        self.ax.yaxis._axinfo["grid"]['color'] =  (1,1,1,0)
        self.ax.zaxis._axinfo["grid"]['color'] =  (1,1,1,0)
        # Make surface plot
        self.ax.plot_surface(theta0_mesh, theta1_mesh, Js, rstride=1,
                cstride=1, cmap='jet', alpha=0.5, linewidth=0)
        self.ax.set_xlabel(r'Intercept($\theta_0$)')
        self.ax.set_ylabel(r'Slope($\theta_1$)')
        self.ax.set_zlabel('Error')        
        self.ax.view_init(elev=30., azim=30)        

    def _get_data(self, models):
        
        paths=[]
        zpaths=[]
        methods = []        
        for k, v in models.items():    
            paths.append(np.array(v.results()['costs'][['theta_0', 'theta_1']]).T)
            zpaths.append(np.array(v.results()['costs']['cost'])) 
            methods.append(k)  
            X = v.results()['X']
            y = v.results()['y']

        return(X, y, paths, zpaths, methods)

    def search(self, models, frames=None, directory=None, filename=None, fps=5):

        X, y, paths, zpaths, methods = self._get_data(models)        
        self._surface(X, y, models)        
        ani = self._anim8(*paths, zpaths=zpaths, methods=methods, frames=frames)
        if directory is not None:
            save_gif(ani, directory, filename, fps)
        return(ani)


# --------------------------------------------------------------------------- #
#                           GRADIENT FIT 3D CLASS                             #   
# --------------------------------------------------------------------------- #
class MultiOptimationFit2D(animation.FuncAnimation):
    """Animates fit for multiple 2D optimizations.""" 

    def __init__(self):
        pass

    def _anim8(self, *paths, methods=[], frames=None, 
                 interval=60, repeat_delay=5, blit=False, **kwargs):

        self.paths = paths

        if frames is None:
            frames = max(path.shape[1] for path in paths)
            self.nth = [1 for p in paths]
        else:
            self.nth = [max(1,math.floor(path.shape[1] / frames)) for path in paths]
        cmap = plt.get_cmap('jet')
        colors = cmap(np.linspace(0,1,len(paths)))
        self.lines = [self.ax.plot([], [], label=method, c=c, lw=2)[0] 
                      for _, method, c in zip_longest(paths, methods, colors)]

        ani = animation.FuncAnimation(self.fig, self._update, init_func=self._init,
                                      frames=frames, interval=interval, blit=blit,
                                      repeat_delay=repeat_delay, **kwargs)
        self.ax.legend(loc='upper left')                                      
        return(ani)                                                  

    def _init(self):
        for line in self.lines:
            line.set_data([], [])
        return self.lines

    def _update(self, i):
        for line, nth, path, in zip(self.lines, self.nth, self.paths):
            print(path)
            line.set_data(*path[::,:i*nth])            
        return self.lines

    def _scatterplot(self, X, y):

        # Designate plot area
        self.fig = plt.figure(figsize=(12, 8))
        self.ax = self.fig.add_subplot(111, projection='3d')
        sns.set(style="whitegrid", font_scale=1)
        sns.scatterplot(x=X[:,1], y=y, ax=self.ax)

        # Set face, tick,and label colors 
        self.ax.set_facecolor('w')
        self.ax.tick_params(colors='k')
        self.ax.xaxis.label.set_color('k')
        self.ax.yaxis.label.set_color('k')
        self.ax.set_ylim(7.5,20)        

        # Set Title, Annotations and label
        title = 'Model Fit'
        self.ax.set_title(title, color='k')
        self.ax.set_xlabel('Living Area')
        self.ax.set_ylabel('Sale Price')
        self.fig.tight_layout()      

    def _get_data(self, models):
        
        paths=[]
        methods = []
        for k, v in models.items():    
            paths.append(np.array(v.results()['costs'][['theta_0', 'theta_1']]).T)
            methods.append(k)  

        return(paths, methods)

    def fit(self, X, y, models, frames=None, directory=None, filename=None, fps=5):

        self._scatterplot(X, y)
        paths, methods = self._get_data(models)
        ani = self._anim8(*paths, methods=methods, frames=frames)
        if directory is not None:
            save_gif(ani, directory, filename, fps)
        return(ani)        