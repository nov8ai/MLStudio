# =========================================================================== #
#                               COST FUNCTIONS                                #
# =========================================================================== #
"""Module contains various cost functions and their gradients. """

import numpy as np

# --------------------------------------------------------------------------- #
#                          Quadratic Cost Function                            #
# --------------------------------------------------------------------------- #
class Quadratic():
    """Quadratic cost function and its gradient."""

    def __init__(self):
        pass

    def cost(self, X, y, theta):
        """Computes quadratic costs for inputs, X, output, y, and parameters, theta.

        Arguments:
        ----------
            X (nd.array): The input variables X for the validation set
            y (1d array): True y variables from validation set
            theta (nd.array): Parameters theta

        Returns:
        -------- 
            Float: Quadratic costs

        """
        e = X.dot(theta) - y
        return(1/2 * np.mean(e**2))

    def gradient(self, X, y, theta):
        """Computes gradient of quadratic cost function n w.r.t. parameters theta. 
        
        Arguments:
        ----------
            X (nd.array): The input variables X for the validation set
            y (1d array): True y variables from validation set
            theta (nd.array): Parameters theta

        Returns:
        -------- 
            nd.array: Partial derivatives w.r.t. parameters theta

        """
        e = X.dot(theta) - y
        return(X.T.dot(e)/X.shape[0])     

class CostFunc():
    """Class returns selected cost function class"""

    def __init__(self):
        """Initializes dictionary of supported cost function classes"""

        self._cost_funcs = {'quadratic': Quadratic()}        

    def get_cost_func(self, func):
        """Returns class corresponding to the requested cost function

        Arguments:
        ----------
            func (str): String containing one of the supported cost functions
        
        Returns:
        --------
            class: Instance of class containing the requested cost function
                implementation
        
        """
        return(self._cost_funcs[func])
