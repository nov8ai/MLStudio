# -*- coding: utf-8 -*-

"""Top-level package for Machine Learning Studio."""

__author__ = """John James"""
__email__ = 'jjames@decisionscients.com'
__version__ = '0.1.0'
