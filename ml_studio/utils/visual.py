# =========================================================================== #
#                                   VISUAL                                    #
# =========================================================================== #
"""Visualization classes"""
# --------------------------------------------------------------------------- #
from IPython.display import HTML
import datetime
import itertools
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')
import matplotlib.animation as animation
import matplotlib.gridspec as gridspec
from itertools import zip_longest
from matplotlib.animation import FuncAnimation
from matplotlib import cm
from matplotlib import animation, rc, rcParams
from matplotlib import colors as mcolors
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import axes3d, Axes3D
import numpy as np
from numpy import array, newaxis
import pandas as pd
import seaborn as sns
from sklearn.model_selection import validation_curve
from sklearn.model_selection import ShuffleSplit
from textwrap import wrap
import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning) 

from .filemanager import save_fig
# --------------------------------------------------------------------------- #
class Visual:
    """Standard interfaces for basic plotting methods""" 
    def __init__(self):
        pass  

    def distplot(self, ax,  data, x, y, z=None, title=None,
                 log=False, xlim=None, ylim=None):

        # Initialize figure and settings        
        sns.set(style="whitegrid", font_scale=1)        

        # Plot time by learning rate 
        ax = sns.distplot(a=data[x])
        ax.set_facecolor('w')
        ax.tick_params(colors='k')
        ax.xaxis.label.set_color('k')
        ax.yaxis.label.set_color('k')
        ax.set_xlabel(x)
        ax.set_ylabel(y)
        ax.set_title(title, color='k')
        # Change to log scale and impose axis limits if requested
        if log: ax.set_xscale('log')
        if ylim is not None: ax.set_ylim(ylim)
        if xlim is not None: ax.set_xlim(xlim)            
        return(ax)             
    
    def scatterplot(self, ax,  data, x, y, z=None, title=None,
                 log=False, xlim=None,  ylim=None):

        # Initialize figure and settings        
        sns.set(style="whitegrid", font_scale=1)        

        # Plot time by learning rate 
        ax = sns.scatterplot(x=x, y=y, hue=z, data=data, ax=ax, legend='full')
        ax.set_facecolor('w')
        ax.tick_params(colors='k')
        ax.xaxis.label.set_color('k')
        ax.yaxis.label.set_color('k')
        ax.set_xlabel(x)
        ax.set_ylabel(y)
        ax.set_title(title, color='k')
        # Change to log scale and impose axis limits if requested
        if log: ax.set_xscale('log')
        if ylim is not None: ax.set_ylim(ylim)
        if xlim is not None: ax.set_xlim(xlim)            
        return(ax)         

    def barplot(self, ax,  data, x, y, z=None, title=None,
                 log=False, xlim=None, ylim=None):

        # Initialize figure and settings        
        sns.set(style="whitegrid", font_scale=1)

        # Plot time by learning rate 
        ax = sns.barplot(x=x, y=y, hue=z, data=data, ax=ax)
        ax.set_facecolor('w')
        ax.tick_params(colors='k')
        ax.xaxis.label.set_color('k')
        ax.yaxis.label.set_color('k')
        ax.set_xlabel(x)
        ax.set_ylabel(y)
        ax.set_title(title, color='k')
        # Change to log scale and impose axis limits if requested
        if log: ax.set_xscale('log')
        if ylim is not None: ax.set_ylim(ylim)
        if xlim is not None: ax.set_xlim(xlim)            
        return(ax)     

    def boxplot(self, ax,  data, x, y, z=None, title=None,
                 log=False, xlim=None, ylim=None):

        # Initialize figure and settings        
        sns.set(style="whitegrid", font_scale=1)  

        # Plot time by learning rate 
        ax = sns.boxplot(x=x, y=y, hue=z, data=data, ax=ax)
        ax.set_facecolor('w')
        ax.tick_params(colors='k')
        ax.xaxis.label.set_color('k')
        ax.yaxis.label.set_color('k')
        ax.set_xlabel(x)
        ax.set_ylabel(y)
        ax.set_title(title, color='k')
        # Change to log scale and limit y-axis if requested
        # Change to log scale and impose axis limits if requested
        if log: ax.set_xscale('log')
        if ylim is not None: ax.set_ylim(ylim)
        if xlim is not None: ax.set_xlim(xlim)            
        return(ax) 

    def lineplot(self, ax, data, x, y, z=None, title=None,
                 log=False, xlim=None, ylim=None):

        # Initialize figure and settings        
        sns.set(style="whitegrid", font_scale=1)

        # Plot time by learning rate 
        ax = sns.lineplot(x=x, y=y, hue=z, data=data, legend='full', ax=ax)
        ax.set_facecolor('w')
        ax.tick_params(colors='k')
        ax.xaxis.label.set_color('k')
        ax.yaxis.label.set_color('k')
        ax.set_xlabel(x)
        ax.set_ylabel(y)
        ax.set_title(title, color='k')
        # Change to log scale and impose axis limits if requested
        if log: ax.set_xscale('log')
        if ylim is not None: ax.set_ylim(ylim)
        if xlim is not None: ax.set_xlim(xlim)            
        return(ax)     

class VisualCV:
    """Plots output of sklearn GridSearchCV results"""

    def scores(self, alg, gs, x, height=1, width=1,  directory=None, 
               log=False, filename=None, show=True):

        # Obtain class containing standard plotting functions
        visual = Visual()

        # Get results and format data in long from
        results = pd.DataFrame.from_dict(gs.cv_results_)
        try:
            param = results.filter(like=x).columns
            param_values = results[param]
        except:
            print("Invalid parameter name")

        scores = results[['mean_train_score', 'mean_test_score']]
        scores = pd.concat([param_values, scores], axis=1)
        scores = pd.melt(scores, id_vars=param, var_name="Dataset", value_name="Scores")

        # Obtain and initialize matplotlib figure
        fig_width = math.floor(12*width)
        fig_height = math.floor(4*height)
        fig, ax = plt.subplots(figsize=(fig_width, fig_height)) 
        
        # Set Title
        title = alg + '\n' + 'GridSearchCV Scores' + '\n' + "Score" + ' By ' + param[0]
        
        # Print line plot
        ax = visual.lineplot(x=param[0], y='Scores', z="Dataset", data=scores, ax=ax, title=title)

        # Change to log scale if requested
        if log: ax.set_xscale('log')
        
        # Finalize, show and save
        fig.tight_layout()
        if show:
            plt.show()
        if directory is not None:
            if filename is None:
                filename = alg + ' GridSearchCV Score Analysis.png'
            save_fig(fig, directory, filename)
        plt.close(fig)             
