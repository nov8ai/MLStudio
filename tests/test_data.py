# %%
# =========================================================================== #
#                                TEST DATA                                    #
# =========================================================================== #
''' Test data module. '''
# --------------------------------------------------------------------------- #
import numpy as np
import pandas as pd

# %%
# --------------------------------------------------------------------------- #
class Ames():
    def __init__(self):
        pass

    def load(self, vars=None, drop_na_columns=False):
        df = pd.read_csv("../gd_studio/tests/test_data_files/ames.csv", encoding="Latin-1", low_memory=False)

        if drop_na_columns:
            missing = df.columns[df.isnull().any()].tolist() 
            df = df.drop(columns=missing)
        
        if vars is not None:
            if set(vars) <= set(list(df.columns)):
                df = df[vars]                
            else:
                raise Exception("Variable(s) is/are not valid.")

        return(df)

