# =========================================================================== #
#                             TEST GRIDSEARCH CV                              #
# =========================================================================== #
''' Tests GridSearch CV'''
#%%
# --------------------------------------------------------------------------- #
import warnings

import numpy as np
import pandas as pd
from scipy import stats
from sklearn.exceptions import DataConversionWarning
from sklearn.datasets import make_regression
from sklearn.metrics import make_scorer
from sklearn.model_selection import GridSearchCV

from ml_studio.supervised_learning.gradient_descent.gradient import BGD, SGD, MBGD
from ml_studio.utils.visual import VisualCV

# --------------------------------------------------------------------------- #
# Suppress warnings
warnings.filterwarnings(action="ignore", category=RuntimeWarning)
warnings.filterwarnings(action='ignore', category=DataConversionWarning)
# --------------------------------------------------------------------------- #
# Load data
X, y = make_regression(n_samples=1000, n_features=1, noise=.1, bias=1, 
                       random_state=50)
# --------------------------------------------------------------------------- #
# Create custom rmse scorer
rmse = make_scorer(BGD().rmse, greater_is_better=False)                       
# --------------------------------------------------------------------------- #
# Set Parameters Grid
learning_rate = np.logspace(-3, 1, endpoint=True)
maxiter = [2500]
precision = [0.001]
i_s = [5]

bgd_params = {'bgd__learning_rate' : learning_rate,
              'bgd__precision' : precision,
              'bgd__maxiter' : maxiter,
              'bgd__epochs_stable' : i_s}
# --------------------------------------------------------------------------- #
# Construct gridsearch
bgd_gs = GridSearchCV(estimator=BGD(),
                    param_grid=bgd_params,
                    scoring=rmse,
                    return_train_score=True,
                    verbose=1,
                    cv=3,
                    error_score=np.nan) 
# --------------------------------------------------------------------------- #
# Fit gridsearch 
bgd_gs.fit(X, y)
#%%
# --------------------------------------------------------------------------- #
# Obtain results
results = pd.DataFrame.from_dict(bgd_gs.cv_results_)
best_scores = results[results.rank_test_score <= 10]
# Best params based on mean test score
print('Best model params: %s' % bgd_gs.cv_results_['params'][bgd_gs.best_index_])
# Best params on hold out set
print('Best holdout params: %s' % bgd_gs.best_params_)
# Best training data accuracy
print('Best training accuracy: %.3f' % bgd_gs.best_score_)
# --------------------------------------------------------------------------- #
# Plot results
visual = VisualCV()
visual.scores(alg="Batch Gradient Descent", gs=bgd_gs, x='learning_rate', log=True)

