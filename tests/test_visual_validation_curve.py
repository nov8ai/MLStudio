# =========================================================================== #
#                             TEST REGRESSION 2D                              #
# =========================================================================== #
''' Tests BGD Regression with single predictor (plus bias term)'''
#%%
# --------------------------------------------------------------------------- #
import warnings

import numpy as np
import pandas as pd
from scipy import stats
from sklearn.exceptions import DataConversionWarning
from sklearn.datasets import make_regression

from ml_studio.utils.preprocessing import RangeScaler, CategoricalEncoder
from ml_studio.supervised_learning.gradient_descent.gradient import BGD, SGD, MBGD
from ml_studio.supervised_learning.gradient_descent.gd_visual import GradientDiagnostics

# --------------------------------------------------------------------------- #
# Suppress warnings
warnings.filterwarnings(action="ignore", category=RuntimeWarning)
warnings.filterwarnings(action='ignore', category=DataConversionWarning)

#%%
# --------------------------------------------------------------------------- #
# Load data
X, y = make_regression(n_samples=1000, n_features=1, noise=.1, bias=1, 
                       random_state=50)
#%%
# --------------------------------------------------------------------------- #
# Preprocessing: Scale features
scaler = RangeScaler()
#X = scaler.fit_transform(X)
#%%
# --------------------------------------------------------------------------- #
# Set Parameters
learning_rate = 0.1
learning_rate_sched = 'c'
decay_rate = 0.5
maxiter = 2500
precision = 0.01
i_s = 2

# Instantiate BGD object and fit data
gd = MBGD(fit_intercept=True,learning_rate=learning_rate, decay_rate=decay_rate,
          learning_rate_sched=learning_rate_sched, maxiter=maxiter, 
          precision=precision, epochs_stable = i_s, early_stop=False)
#%%%
gd.fit(X,y)          
viz = GradientDiagnostics()
fig = viz.validation_curve(model=gd)