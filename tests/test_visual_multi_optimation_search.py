# =========================================================================== #
#                             TEST REGRESSION 2D                              #
# =========================================================================== #
''' Tests BGD Regression with single predictor (plus bias term)'''
#%%
# --------------------------------------------------------------------------- #
import warnings

from IPython.display import HTML, Image
from matplotlib import rc, rcParams
import numpy as np
import pandas as pd
from scipy import stats
from sklearn.exceptions import DataConversionWarning
from sklearn.datasets import make_regression

from ml_studio.utils.preprocessing import RangeScaler, CategoricalEncoder
from ml_studio.supervised_learning.gradient_descent.gradient import BGD, SGD, MBGD
from ml_studio.supervised_learning.gradient_descent.gd_visual import MultiOptimationSearch3D

# --------------------------------------------------------------------------- #
# Suppress warnings
warnings.filterwarnings(action="ignore", category=RuntimeWarning)
warnings.filterwarnings(action='ignore', category=DataConversionWarning)

#%%
# --------------------------------------------------------------------------- #
# Load data
X, y = make_regression(n_samples=1000, n_features=1, noise=20, random_state=55)

#%%
# --------------------------------------------------------------------------- #
# Set Parameters
learning_rate = 0.1
learning_rate_sched = 'c'
decay_rate = 0.5
maxiter = 2500
precision = 0.01
i_s = 2
# --------------------------------------------------------------------------- #
# Instantiate Models object and fit data
bgd = BGD(fit_intercept=True,learning_rate=learning_rate, decay_rate=decay_rate,
          learning_rate_sched=learning_rate_sched, maxiter=maxiter, 
          precision=precision, epochs_stable = i_s, early_stop=True, random_state=50)
sgd = SGD(fit_intercept=True,learning_rate=learning_rate, decay_rate=decay_rate,
          learning_rate_sched=learning_rate_sched, maxiter=maxiter, 
          precision=precision, epochs_stable = i_s, early_stop=True, random_state=50)
mbgd = MBGD(fit_intercept=True,learning_rate=learning_rate, decay_rate=decay_rate,
          learning_rate_sched=learning_rate_sched, maxiter=maxiter, 
          precision=precision, epochs_stable = i_s, early_stop=True, random_state=50)
#%%%
# --------------------------------------------------------------------------- #
# Fit models
models = {}
for model, name in zip((bgd, sgd, mbgd), 
                    ('Batch Gradient Descent', 
                        'Stochastic Gradient Descent',
                        'Mini-Batch Gradient Descent')):
    model.fit(X, y)                        
    models[name] = model

# --------------------------------------------------------------------------- #
# Create animation
directory = "./tests/test_figures/"
filename = "test_multi_optimation_fit.gif"
viz = MultiOptimationSearch3D()
ani = viz.search(models=models, directory=directory, filename=filename)
rc('animation', html='jshtml')
rc
HTML(ani.to_jshtml())